### Install a Linux VM

Download and install UTM from here: https://mac.getutm.app/

Install Debian Minimal on UTM from here: https://mac.getutm.app/gallery/debian-10-4-minimal

Right click the newly installed image and choose Edit.
![edit.png](./edit.png)

Under the System tab, Setup VM for at least 4 Gig of memory, 2 cores

![config-vm1.png](./config-vm1.png)

Setup up networking: Choose Bridged

![vm-bridged.png](./vm-bridged.png)
Boot the VM

Determine your IP address of your VM

```
ip addr
```

On my install the reachable IP from MacOS was interface `enp0s5`


You should now be able to ssh to your VM directly:

```
ssh debian@[YOUR VM IP]
```

(password `debian`)


### Install Emacs or some editor

In the VM

```
sudo apt-get install emacs-nox
```

Or install whatever editor you prefer.



### Install official Docker for ARM64

In the VM - run these commands

```
sudo apt-get update --allow-releaseinfo-change

sudo apt-get install apt-transport-https ca-certificates curl gnupg lsb-release

sudo apt-get update --allow-releaseinfo-change

sudo apt-get remove docker docker-engine docker.io containerd runc

sudo apt-get update

curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo   "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update --allow-releaseinfo-change

sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin


```


### Install mini-kube

In the VM

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-arm64

sudo install minikube-linux-arm64 /usr/local/bin/minikube
```

### Install NFS server.

This is so you can edit files on your Mac that are residing in your VM


In the VM...
```
sudo apt-get install nfs-kernel-server

```

### Configure NFS and mount from MacOS

In the VM...

Edit the `/etc/exports` file

```
sudo emacs -nw /etc/exports
```

Add the following line:
```
/home/debian      *(rw,async,no_subtree_check,all_squash,anonuid=1000,anongid=1000)
```

For better security you can change `*` to the IP address of your desktop.

And reload the server

```
sudo /etc/init.d/nfs-kernel-server reload
```

### Verify the mount is exported

In MacOS, from a terminal, run the following:

```
showmount -e [IP of your VM]
```

Should show results similar to:
```
user@MacBook-Pro ~ % showmount -e 192.168.1.90
Exports list on 192.168.1.90:
/home/debian                        *
```

### Mount the volume

First, in MacOS create a mount point:

```
mkdir ~/debian-work
```

Create a bash script which mounts the volume. Open an editor and create a file `mount-debian.sh` in your home folder.

Replace `192.168.1.90` with the IP of your VM - as discovered earlier.

```
#!/bin/bash

IP="${IP:-192.168.1.90}"

mount -v -t nfs -o resvport,async,soft,nolocks,vers=3,noatime,nfc,actimeo=1 ${IP}:/home/debian $HOME/debian-work
```

(we have found these options to work best on most recent MacOS - including Big Sur and Monterey)

then

```
chmod a+x ~/mount-debian.sh

```
And run it...
```
sudo ~/mount-debian.sh
```

Result similar to:
```
 sudo ./mount-debian.sh
192.168.1.90:/home/debian on /Users/user/debian-work (nfs, asynchronous, noatime)
```

Your home folder in your UTM VM is now accessible from MacOS


